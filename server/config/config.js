/***************************
 * PUERTO
 ***************************/
process.env.PORT = process.env.PORT || 3000;

/***************************
 * ENTORNO
 ***************************/
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

/***************************
 * Vencimiento del Token
 ***************************/
process.env.EXPIRE_TOKEN = '48h';

/***************************
 * SEED de autenticacion
 ***************************/
process.env.SEED = process.env.SEED || 'SEED de token DEV';


/***************************
 * BASE DE DATOS
 ***************************/
let urlDB;

if (process.env.NODE_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/cafe';
} else {
    urlDB = process.env.MONGO_URI;
}

process.env.URL_DB = urlDB;


/***************************
 * Google Client ID
 ***************************/
process.env.CLIENT_ID = process.env.CLIENT_ID || '395677753591-vsj774q63uuqsvsi7d0brap2co5nmjg6.apps.googleusercontent.com';