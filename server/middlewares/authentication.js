const jwt = require('jsonwebtoken');
const User = require('../models/user');

/*********************
 * Verificar Token
 *********************/
let checkToken = (req, res, next) => {
    let token = req.get('token');
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err
            });
        }

        req.user = decoded.user;
        next();
    });
};

/*********************
 * Verificar ADMIN_ROLE
 *********************/
let checkAdminRole = (req, res, next) => {
    if (req.user.role !== 'ADMIN_ROLE') {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'No tiene los permisos necesarios'
            }
        });
    }

    next();
};

/*********************
 * Verificar token de imagen
 *********************/
let checkTokenImg = (req, res, next) => {
    let token = req.query.token;
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err
            });
        }

        req.user = decoded.user;
        next();
    });
};

module.exports = {
    checkToken,
    checkAdminRole,
    checkTokenImg
}