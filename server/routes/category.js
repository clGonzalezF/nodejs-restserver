const express = require('express');
const app = express();
const _ = require('underscore');
const { checkToken, checkAdminRole } = require('../middlewares/authentication');

let Category = require('../models/category');

/***************************
 * Mostrar todas las categorias
 ***************************/
app.get('/category', checkToken, (req, res) => {
    Category.find({}) //Filtrar que datos requiero
        .sort('name')
        .populate('user', 'name email')
        //.populate({ path: 'user', model: User, select: 'name email' })
        .exec((err, categories) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                categories
            });
        });
});

/***************************
 * Mostrar una categoria por ID
 ***************************/
app.get('/category/:id', checkToken, (req, res) => {
    let id = req.params.id;

    Category.findById(id, (err, categoryDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!categoryDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Categoria no existe"
                }
            });
        }

        res.json({
            ok: true,
            category: categoryDB
        });
    });
});

/***************************
 * Crear nueva categoria
 ***************************/
app.post('/category', checkToken, (req, res) => {
    let body = req.body;
    let idUser = req.user._id;

    let category = new Category({
        name: body.name,
        user: req.user._id,
    });

    category.save((err, categoryDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!categoryDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            category: categoryDB
        });
    });
});

/***************************
 * Actualizar categoria
 ***************************/
app.put('/category/:id', checkToken, (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, ['name']) //req.body;

    Category.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, categoryDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!categoryDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            category: categoryDB,
            update: true
        });
    });
});

/***************************
 * Borrar nueva categoria
 ***************************/
app.delete('/category/:id', [checkToken, checkAdminRole], (req, res) => {
    let id = req.params.id;
    Category.findByIdAndRemove(id, (err, categoryDelete) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!categoryDelete) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Categoria no existe"
                }
            });
        }

        res.json({
            ok: true,
            category: categoryDelete
        });
    });
});

module.exports = app;