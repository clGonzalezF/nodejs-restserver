const express = require('express');
const app = express();
const { checkToken } = require('../middlewares/authentication');
const _ = require('underscore');

let Product = require('../models/product');

/***************************
 * Mostrar todos los productos
 ***************************/
app.get('/product', checkToken, (req, res) => {
    req.query.desde = parseInt(req.query.desde);
    req.query.limite = parseInt(req.query.limite);
    let desde = (Number.isInteger(req.query.desde)) ? req.query.desde : 0;
    let limite = (Number.isInteger(req.query.limite) && req.query.limite <= 200) ? req.query.limite : 6;

    Product.find({ available: true }) //Filtrar que datos requiero
        .skip(desde)
        .limit(limite)
        .sort('name')
        .populate('user', 'name')
        .populate('category', 'name')
        //.populate({ path: 'user', model: User, select: 'name email' })
        .exec((err, product) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Product.countDocuments({ available: true }, (err, conteo) => {
                res.json({
                    ok: true,
                    product,
                    conteo
                });
            });

        });
});

/***************************
 * Mostrar un producto por ID
 ***************************/
app.get('/product/:id', checkToken, (req, res) => {
    let id = req.params.id;

    Product.findById(id)
        .populate('user', 'name')
        .populate('category', 'name')
        .exec((err, productDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            if (!productDB) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: "Producto no existe"
                    }
                });
            }

            res.json({
                ok: true,
                product: productDB
            });
        });
});

/***************************
 * Buscar productos
 ***************************/
app.get('/product/search/:filter', checkToken, (req, res) => {
    let filter = req.params.filter;
    let regexp = new RegExp(filter, 'i');

    Product.find({ name: regexp })
        .populate('category', 'name')
        .exec((err, productDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            if (!productDB) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: "Producto no existe"
                    }
                });
            }

            res.json({
                ok: true,
                product: productDB
            });
        });
});

/***************************
 * Crear un producto
 ***************************/
app.post('/product', checkToken, (req, res) => {
    let body = req.body;

    let product = new Product({
        user: req.user._id,
        name: body.name,
        unitPrice: body.unitPrice,
        description: body.description,
        category: body.category,
        available: body.available
    });

    product.save((err, productDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.status(201).json({
            ok: true,
            product: productDB
        });
    });
});

/***************************
 * Actualizar producto
 ***************************/
app.put('/product/:id', checkToken, (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, ['user', 'name', 'unitPrice', 'description', 'category', 'available'])

    Product.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, productDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            product: productDB,
            update: true
        });
    });
});

/***************************
 * Borrar nueva categoria
 ***************************/
app.delete('/product/:id', checkToken, (req, res) => {
    let id = req.params.id;
    let body = { available: false };

    Product.findByIdAndUpdate(id, body, { new: true }, (err, productDelete) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!productDelete) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Producto ya no se encuentra disponible"
                }
            });
        }

        res.json({
            ok: true,
            product: productDelete
        });
    });
});

module.exports = app;