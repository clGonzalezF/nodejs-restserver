const express = require('express');
const app = express();
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { OAuth2Client } = require('google-auth-library');

const client = new OAuth2Client(process.env.CLIENT_ID);

app.post('/login', (req, res) => {
    let body = req.body;
    if (typeof body.email == 'undefined' || body.email == '' || body.email == null ||
        typeof body.password == 'undefined' || body.password == '' || body.password == null) {
        return res.status(400).json({
            ok: false,
            err: {
                message: "Debe ingresar Usuario y contraseña"
            }
        });
    }

    User.findOne({ email: body.email }, (err, userDB) => {
        console.log(body);
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!userDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Usuario* o contraseña incorrectos"
                }
            });
        }

        if (!bcrypt.compareSync(body.password, userDB.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Usuario o contraseña* incorrectos"
                }
            });
        }

        let token = jwt.sign({
            user: userDB, //PAYLOAD
        }, process.env.SEED, { expiresIn: '1h' });

        res.json({
            ok: true,
            user: userDB,
            token
        });
    });
});

//Configuracion de google
async function verify(gtoken) {
    const ticket = await client.verifyIdToken({
        idToken: gtoken,
        audience: process.env.CLIENT_ID
    });
    const payload = ticket.getPayload();

    return {
        name: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    }
}
verify().catch(console.log('Google error'));

app.post('/google', async(req, res) => {
    let gtoken = req.body.google_token;

    let googleUser = await verify(gtoken)
        .catch(e => {
            return res.status(403).json({
                ok: false,
                err: e
            });
        });

    User.findOne({ email: googleUser.email }, (err, userDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        if (userDB) {
            if (userDB.google === false) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'Debe autenticase normalmente, no por Google'
                    }
                });
            } else {
                let token = jwt.sign({
                    user: userDB, //PAYLOAD
                }, process.env.SEED, { expiresIn: '1h' });

                return res.json({
                    ok: true,
                    user: userDB,
                    token
                });
            }
        } else {
            //Usuario no existe en DB
            let user = new User();
            user.name = googleUser.name;
            user.email = googleUser.email;
            user.img = googleUser.img;
            user.google = true;
            user.password = ':)';

            user.save((err, userDB) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                let token = jwt.sign({
                    user: userDB, //PAYLOAD
                }, process.env.SEED, { expiresIn: '1h' });

                return res.json({
                    ok: true,
                    user: userDB,
                    token
                });
            });
        }
    });

    /*
    res.json({
        user: googleUser
    });
    */
});

module.exports = app;