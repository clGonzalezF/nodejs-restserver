const express = require('express');
const fs = require('fs');
const path = require('path');
const { checkToken, checkTokenImg } = require('../middlewares/authentication');

const app = express();

app.get('/image/:type/:img', checkTokenImg, (req, res) => {
    let type = req.params.type;
    let img = req.params.img;
    let pathImgDefault = path.resolve(__dirname, '../assets/img');
    let pathImg = path.resolve(__dirname, '../../uploads/' + type);

    if (fs.existsSync(pathImg + '/' + img)) {
        res.sendFile(pathImg + '/' + img);
    } else {
        res.sendFile(pathImgDefault + '/default-img.jpeg');
    }
});

module.exports = app;