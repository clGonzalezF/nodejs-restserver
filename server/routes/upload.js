const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const User = require('../models/user');
const Product = require('../models/product');
const path = require('path');
const fs = require('fs');

// default options
app.use(fileUpload()); // > req.files

app.put('/upload/:type/:id', function(req, res) {
    let type = req.params.type;
    let id = req.params.id;

    //Validar tipo
    let validTypes = ['user', 'product'];
    if (validTypes.indexOf(type) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                mesagge: 'Solo se permiten archivos para usuarios y productos: ',
                validTypes
            }
        });
    }

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'No se selecciono archivo'
            }
        });
    }

    let myfile = req.files.myfile; //name en un form

    //Extensiones permitidas
    //let validExt = ['png', 'jpg', 'jpeg', 'gif'];
    let validMime = ['image/gif', 'image/jpeg', 'image/png'];
    let mimeType = myfile.mimetype;

    let nameFile = myfile.name.split('.')[0];
    let extFile = myfile.name.split('.')[1];

    if (validMime.indexOf(mimeType) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                mesagge: 'Solo se permiten archivos PNG, JPG/JPEG y GIF',
                ext: extFile
            }
        });
    }

    //Cambiar nombre de archivo
    let newNameFile = `${ id }-${ new Date().getMilliseconds() }.${ extFile }`;
    let pathImg = path.resolve(__dirname, '../../uploads/' + type);

    myfile.mv(`${ pathImg }/${ newNameFile }`, function(err) {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (type == validTypes[0]) { //user
            imgUser(id, res, newNameFile, pathImg);
        } else { //products
            imgProduct(id, res, newNameFile, pathImg);
        }
    });
});

function imgUser(id, res, newNameFile, pathImg) {
    User.findById(id, (err, userDB) => {
        if (err) {
            deleteFile(pathImg, newNameFile);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!userDB) {
            deleteFile(pathImg, newNameFile);
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Usuario no existe"
                }
            });
        }

        deleteFile(pathImg, userDB.img);

        userDB.img = newNameFile;
        userDB.save((err, userSaved) => {
            return res.json({
                ok: false,
                userSaved,
                message: 'Archivo subido satisfactoriamente',
            });
        });
    });
}

function imgProduct(id, res, newNameFile, pathImg) {
    Product.findById(id, (err, productDB) => {
        if (err) {
            deleteFile(pathImg, newNameFile);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productDB) {
            deleteFile(pathImg, newNameFile);
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Producto no existe"
                }
            });
        }

        deleteFile(pathImg, productDB.img);

        productDB.img = newNameFile;
        productDB.save((err, productSaved) => {
            return res.json({
                ok: false,
                productSaved,
                message: 'Archivo subido satisfactoriamente',
            });
        });
    });
}

function deleteFile(tmpPath, tmpFile) {
    if (fs.existsSync(tmpPath + '/' + tmpFile)) {
        fs.unlinkSync(tmpPath + '/' + tmpFile); //Borrar archivo
        //console.log(userDB.img + ' :: BORRADA')
    }
}

module.exports = app;