const express = require('express');
const User = require('../models/user');
const app = express();
const bcrypt = require('bcrypt');
const _ = require('underscore');
const { checkToken, checkAdminRole } = require('../middlewares/authentication');

//Obtener usuarios 
app.get('/usuario', checkToken, function(req, res) {
    req.query.desde = parseInt(req.query.desde);
    req.query.limite = parseInt(req.query.limite);
    let desde = (Number.isInteger(req.query.desde)) ? req.query.desde : 0;
    let limite = (Number.isInteger(req.query.limite) && req.query.limite <= 200) ? req.query.limite : 5;

    User.find({ status: true }, 'name email role status google img') //Filtrar que datos requiero
        .skip(desde)
        .limit(limite)
        .exec((err, users) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            User.countDocuments({ status: true }, (err, conteo) => {
                res.json({
                    ok: true,
                    users,
                    conteo: conteo
                });
            });
        });
});

//Crear nuevo usuario 
app.post('/usuario', [checkToken, checkAdminRole], function(req, res) {
    let body = req.body;

    let user = new User({
        name: body.name,
        email: body.email,
        password: (typeof body.password != 'undefined') ? bcrypt.hashSync(body.password, 10) : null,
        role: body.role
    });

    user.save((err, userDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            user: userDB
        });
    });
});

//Actualizar Datos de un usuario
app.put('/usuario/:id', [checkToken, checkAdminRole], function(req, res) {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'email', 'img', 'role', 'status']) //req.body;

    User.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, userDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            user: userDB
        });
    });
});

//Borrar un usuario (Deshabilitarlo)
app.delete('/usuario/:id', [checkToken, checkAdminRole], function(req, res) {
    let id = req.params.id;
    let body = { status: false };
    //User.findByIdAndRemove(id, (err, userDelete) => {
    User.findByIdAndUpdate(id, body, { new: true }, (err, userDelete) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!userDelete) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Usuario no existe"
                }
            });
        }

        res.json({
            ok: true,
            user: userDelete
        });
    });
});

module.exports = app;