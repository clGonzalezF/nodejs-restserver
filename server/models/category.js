const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let categorySchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, 'El nombre (name) es obligatorio']
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
            //required: [true, 'El id del creador (idCreator) es obligatorio']
    }
});

module.exports = mongoose.model('category', categorySchema);